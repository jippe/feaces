#!/usr/bin/env python3

"""
dit is een pipe_line van alle programma's die we hebben gedaan
waar een input gegeven wordt en er iets anders uitkomt.
"""
import os
import time
import argparse
import sys
import orf_finder

DEFAULT_INPUT_FOLDER = '/commons/Themas/Thema06/Themaopdracht/feaces/fastQ_data'
DEFAULT_OUTPUT_FOLDER = "/data/storage/students/2018-2019/Thema06/feaces/output"

class Patient(object):
    """
    pipe line van het process.
    """
    def __init__(self, inputfolder, inputfile, outputfolder):
        """
        hier wordt de variabele input_folder als default_input_folder,
        de output_folder als de output van de functie make_output_folder
        en file_name als de output van de get_file_name functie meegegeven.
        ook worden hier de reverse en de forward files (de gesplitted files benoemd.
        """
        self.input_folder = inputfolder
        self.file_name = inputfile[0:3]
        self.full_file_name = inputfile
        self.output_folder = self.make_output_folders(outputfolder)
        self.split_files()
        self.forward = self.forward
        self.reverse = self.reverse
        self.directory = os.getcwd()

    def make_output_folders(self, outputfolder):
        """
        hier wordt de output folders aangemaakt.
        :return:
        """
        # hier wordt de centrale output folder aangemaakt.
        #os.system('mkdir /data/storage/students/2018-2019/Thema06/feaces/{}'
        #          .format(self.file_name))

        self.output_folder = '{}/{}'.format(outputfolder, self.file_name)
        print(self.output_folder)

        os.system('mkdir {}'.format(outputfolder))

        os.system('mkdir {}'.format(self.output_folder))

        # maakt een mapje voor de gesplitte files.
        os.system('mkdir {}/splitted_files/'.format(self.output_folder))

        # maakt een mapje aan voor de quality control van de original files.
        os.system('mkdir {}/origin_fastqc/'.format(self.output_folder))

        # hier wordt een folder voor trimmomatic aangemaakt.
        os.system('mkdir {}/trimmomatics/'.format(self.output_folder))

        # hier wordt een quality control mapje aangemaakt voor de files na de trimmomatic
        os.system('mkdir {}/quality_control/'.format(self.output_folder))

        # maakt een mapje aan voor de spades output
        os.system('mkdir {}/{}'.format(self.output_folder, self.file_name))

        # maakt een tweede mapje aan voor de bowtie
        os.system('mkdir {}/bowtie'.format(self.output_folder))

        # maakt een mapje aan voor de blast output virulent
        os.system('mkdir {}/blastoutput_vir'.format(self.output_folder))

        # maakt een mapje aan voor de uitput van de samtools.
        os.system('mkdir {}/samtools'.format(self.output_folder))

        # maakt een mapje aan voor de uitput van de bedtools.
        os.system('mkdir {}/bedtools'.format(self.output_folder))

        # maakt een mapje aan voor de uitput van de orf_finder.
        os.system('mkdir {}/orf_finder_results'.format(self.output_folder))

        return self.output_folder

    def split_files(self):
        """
        hier split hij de files en schrijft hij twee files een is de reverse en de ander de forward.
        :return:
        """
        start = time.time()
        forward_name = "{}/splitted_files/{}_forward.fastq"\
            .format(self.output_folder, self.file_name)
        reverse_name = '{}/splitted_files/{}_reverse.fastq'\
            .format(self.output_folder, self.file_name)

        forward = True
        file = open("{}/{}".format(self.input_folder, self.full_file_name))
        forward_file = open(forward_name, "w")
        reverse_file = open(reverse_name, "w")
        for i, line in enumerate(file):
            if forward:
                forward_file.write(line)
            else:
                reverse_file.write(line)
            if not (i + 1) % 4 and not forward:
                forward = True
            elif not (i + 1) % 4 and forward:
                forward = False

        file.close()
        print(time.time() - start)
        self.forward = forward_name
        self.reverse = reverse_name

        return self.forward, self.reverse

    def origin_file_fastqc(self):
        """
        maakt eerst een mapje aan waar alles in komt.
        hier doet hij quality controlop de forward and reverse fastq bestanden.
        :return:
        """
        start = time.time()

        os.system(
            'fastqc {}/splitted_files/{}_forward.fastq'
            .format(self.output_folder, self.file_name))
        os.system(
            'fastqc {}/splitted_files/{}_reverse.fastq'
            .format(self.output_folder, self.file_name))

        os.system(
            'mv {}/splitted_files/{}_forward_fastqc.html {}/origin_fastqc/'
            .format(self.output_folder, self.file_name, self.output_folder))

        os.system(
            'mv {}/splitted_files/{}_reverse_fastqc.html {}/origin_fastqc/'
            .format(self.output_folder, self.file_name, self.output_folder))

        os.system(
            'mv {}/splitted_files/{}_reverse_fastqc.zip {}/origin_fastqc/'
            .format(self.output_folder, self.file_name, self.output_folder))

        os.system(
            'mv {}/splitted_files/{}_forward_fastqc.zip {}/origin_fastqc/'
            .format(self.output_folder, self.file_name, self.output_folder))
        print(time.time() - start)
        return 0
    # trimmomatic
    # geef de files

    def trimmomatic(self):
        """
        trimmomatic op de forward strand en de reverse strand.
        :return:
        """
        start = time.time()
        trimmo = 'trimmomatic-0.38.jar PE -phred33 {}/splitted_files/' \
                 '{}_forward.fastq {}/splitted_files/{}_reverse.fastq ' \
                 '{}/trimmomatics/{}_forward_paired.fastq {}/trimmomatics/' \
                 '{}_forward_unpaired.fastq '\
                 '{}/trimmomatics/{}_reverse_paired.fastq {}/trimmomatics/' \
                 '{}_reverse_unpaired.fastq/'\
                 ' ILLUMINACLIP:adapters/TruSeq3-PE.fa:2:30:10 MINLEN:70 ' \
                 'TRAILING:20'.format(self.output_folder, self.file_name,
                                      self.output_folder, self.file_name,
                                      self.output_folder, self.file_name,
                                      self.output_folder, self.file_name,
                                      self.output_folder, self.file_name,
                                      self.output_folder, self.file_name)
        # voert de trimmo uit
        os.system(trimmo)
        print(time.time() - start)
        return 0

    # quality control
    def quality_control(self):
        """
        voert de fastqc uit op de forward en reverse strand nadat de trimmomatic erop is uitgevoerd.
        :return:
        """
        start = time.time()

        os.system('fastqc {}/trimmomatics/{}_forward_paired.fastq'
                  .format(self.output_folder, self.file_name))

        os.system('fastqc {}/trimmomatics/{}_reverse_paired.fastq'
                  .format(self.output_folder, self.file_name))

        os.system('mv {}/trimmomatics/{}_forward_paired_fastqc.html {}/quality_control/'
                  .format(self.output_folder, self.file_name, self.output_folder))

        os.system('mv {}/trimmomatics/{}_reverse_paired_fastqc.html {}/quality_control/'
                  .format(self.output_folder, self.file_name, self.output_folder))

        os.system('mv {}/trimmomatics/{}_forward_paired_fastqc.zip {}/quality_control/'
                  .format(self.output_folder, self.file_name, self.output_folder))

        os.system('mv {}/trimmomatics/{}_reverse_paired_fastqc.zip {}/quality_control/'
                  .format(self.output_folder, self.file_name, self.output_folder))

        print(time.time() - start)
        return 0

    def bowtie(self, cores=20):
        """
        bowtie op de forwared_paired file.
        :return:
        """
        start = time.time()

        os.system('bowtie2 -x /data/storage/student/2018-2019/Thema06/feaces/'
                  'Homo_sapiens/UCSC/hg38/Sequence/Bowtie2Index/'
                  'genome -1 {}/trimmomatics/{}_forward_paired.fastq'
                  ' -2 {}/trimmomatics/{}_reverse_paired.fastq -p {} -S {}/{}_output.sam'
                  .format(self.output_folder, self.file_name,
                          self.output_folder, self.file_name,
                          cores,
                          self.output_folder, self.file_name))

        os.system('mv {}/{}_output.sam {}/bowtie/'.format(self.output_folder, self.file_name,
                                                          self.output_folder))
        print(time.time() - start)
        return 0

    def samtools_make_bam(self):
        """
        maakt een bam file van de sam file.
        :return:
        """
        os.system('samtools view -bS {}/bowtie/{}_output.sam > {}/samtools/{}_output.bam'
                  .format(self.output_folder, self.file_name,
                          self.output_folder, self.file_name))
        return 0

    def samtools_unmapped(self):
        """
        maakt een ummapped output bam van de output bam file.
        :return:
        """
        os.system('samtools view -b -f 12 -F 256 {}/samtools/{}_output.bam > '
                  '{}/samtools/{}_output_unmapped.bam'
                  .format(self.output_folder,
                          self.file_name,
                          self.output_folder,
                          self.file_name))
        return

    def samtools_sorting(self):
        """
        sorteerd de unmapped output bam file.
        :return:
        """
        os.system('samtools sort -n {}/samtools/{}_output_unmapped.bam > '
                  '{}/samtools/{}_output_unmapped_sorted.bam'
                  .format(self.output_folder,
                          self.file_name,
                          self.output_folder,
                          self.file_name))
        return 0

    def bedtools_bam_to_fq(self):
        """
        verwijderd het menselijk dna uit de file.
        :return:
        """
        os.system('bedtools bamtofastq -i {}/samtools/{}_output_unmapped_sorted.bam -fq '
                  '{}/bedtools/{}_host_removed_forward.fastq -fq2 '
                  '{}/bedtools/{}_host_removed_reverse.fastq'
                  .format(self.output_folder, self.file_name,
                          self.output_folder, self.file_name,
                          self.output_folder, self.file_name))
        return 0

    def assembler(self, cores=20):
        """
        voert spades uit op de forward en reverse files.
        :return:

        # hij laat geen K77 in

        """
        start = time.time()
        os.system('spades -o {}/{} -1 {}/bedtools/{}_host_removed_forward.fastq -2 '
                  '{}/bedtools/{}_host_removed_reverse.fastq -t {} -m 250 --meta'.
                  format(self.output_folder, self.file_name, self.output_folder,
                         self.file_name, self.output_folder, self.file_name, cores))
        print(time.time() - start)
        return 0

    def virulente_blast(self):
        """
        blast de contigs tegen alle virulenten genomen aan
        :return:
        """
        os.system('blastn -db VFDB_setB_nt.fas -query  {}/{}/K55/final_contigs.fasta -out '
                  '{}/blastoutput_vir/{}_blastoutput_vir -outfmt 6'
                  .format(self.output_folder, self.file_name,
                          self.output_folder, self.file_name))

        try:
            os.system('mv {}/{}_blastoutput_vir {}/blastoutput_vir/'
                      .format(self.directory, self.file_name,
                              self.output_folder))

        except FileNotFoundError:
            print('no matches in virulent')

        return 0


    def orf_finder(self, cores=20):
        """
        voert de functies uit van orf_finder en print de tijd dat het duurt +
        alle values uit de items van de orflists.
        :return:
        """
        start = time.time()

        fasta_file = "{}/{}/K55/final_contigs.fasta".format(self.output_folder,
                                                            self.file_name)
        output_name = "{}/orf_finder_results/{}".format(self.output_folder, self.file_name)
        sequence_headers_l = orf_finder.open_file(fasta_file)

        seq_list = orf_finder.find_orf_multicore(cores, sequence_headers_l)

        orf_finder.create_output_file(output_name, seq_list[0][0], seq_list[0][1])

        end = time.time()
        react_time = end - start
        print('it took the orf_finder', react_time)

        return 0


def get_file_name():
    """
    hier kun je de filename opgeven
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", default=DEFAULT_INPUT_FOLDER,
                        help="insert the filename of the input file")
    parser.add_argument("--output", default=DEFAULT_OUTPUT_FOLDER,
                        help="insert the filename of the input file")
    parser.add_argument("--cores", type=int, default=20,
                        help="amount of cores available")
    args = parser.parse_args()
    return args

def main():
    """
    voert alles op volgorde uit
    :return:
    """
    args = get_file_name()
    for file in os.listdir(args.input):
        # uitvoeren
        if file.endswith('.fastq'):
            pipe_line = Patient(args.input, file, args.output)

            print('splitting')
            pipe_line.split_files()

            print('quality original file')
            pipe_line.origin_file_fastqc()

            print('trimmen')
            pipe_line.trimmomatic()

            print('quality')
            pipe_line.quality_control()

            print('bowtie')
            pipe_line.bowtie(args.cores)

            print('samtools_make_bam')
            start = time.time()
            pipe_line.samtools_make_bam()

            print('samtools_unmapped')
            pipe_line.samtools_unmapped()

            print('samtools_sorting')
            pipe_line.samtools_sorting()

            print('bedtools_bam_to_fq')
            pipe_line.bedtools_bam_to_fq()
            print(time.time() - start)

            print('assembling')
            pipe_line.assembler(args.cores)

            print('orf_finder')
            pipe_line.orf_finder(args.cores)

            print('blasten virulent')
            pipe_line.virulente_blast()

    return 0


if __name__ == '__main__':
    sys.exit(main())
