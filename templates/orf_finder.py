#!/usr/bin/env python3

"""
This programme will serve as an ORF finder so that we may locate genes.
USAGE:
orf_finder.py [file] [outputname-prefix] [number of cores you want to use]
"""

import argparse
import re
import time
from multiprocessing import Pool


def arguments():
    """
    zorgt ervoor dat je een fasta_file kunt opgeven.
    :return: fasta_file ongeopend
    """

    parser = argparse.ArgumentParser("Read all the arguments")
    parser.add_argument("files", help="Argument that takes the file")
    parser.add_argument("outputname", help="The name of the output,"
                                           " may be very short, things "
                                           "will be added to it for clarity.")
    parser.add_argument("cores", help="The amount of cores.", type=int)
    args = parser.parse_args()
    return args


def open_file(fasta_file):
    """
    opent de file en haalt de sequentie eruit
    :return: de sequentie in een string
    """
    sequence = ''
    contigs_l = []
    header = ">test"
    op_file = open(fasta_file)
    for line in op_file:
        if line.startswith(">"):
            header = line
            contigs_l.append([sequence.replace('\n', ''), header])
            sequence = ""
        else:
            sequence += line
    contigs_l.append([sequence.replace('\n', ''), header])
    op_file.close()
    return contigs_l[1:]


def makeindexlist(regexstart, regexstop, sequence):
    """
    short function to find the indexes of start and stopcodons in a sequence
    returns a list of startcodon locations and a list of stopcodon locations
    """
    # This list comprehension finds the locations of start and stop sequences and
    indexlist = [x.start() for x in regexstart.finditer(sequence)]
    indexliststop = [x.start() for x in regexstop.finditer(sequence)]
    # returns a list containing 2 lists, the startcodon locations and the stopcodon locations.
    return [indexlist, indexliststop]


def createframelists(indexlist):
    """
    creates the 3 different reading frames from the lists, separates them
    based on being divisable by 3 in different restamounts, using % == 0/1/2
    returns a list of 3 lists which carry the codon locations according
    to the different reading frames.
    """

    start1 = []
    start2 = []
    start3 = []
    # All items that are divisible by 3 are the first frame,
    # divisble by 3 rest 1 are the second frame, etc.
    for item in indexlist:
        if item % 3 == 0:
            start1.append(item)
        elif item % 3 == 1:
            start2.append(item)
        elif item % 3 == 2:
            start3.append(item)
    startlists = [start1, start2, start3]
    # returns a list with 3 lists containing the locations.
    return startlists


def findspans(sequence, indexliststart, indexliststop):
    """
    This function will put the location of the orfs in a list and for
    this requires the start and stop codon location lists.
    returns a single list with the spans according to that frame
    the function assumes that the lists it receives are IN FRAME
    The counter increases itself by 1 every loop and checks the startcodonlist
    """
    orflist_spans = []
    counter = 0
    # a counter is utilized to make sure that when an orf is found,
    # another startcodon INSIDE of the orf cannot be used as another starting point.
    while counter < len(sequence):
        if len(indexliststop) == 0:
            break
        # if counter is longer than the lenght of the sequence, it stops.
        elif counter in indexliststart:
            # Now we loop the stopindex until we find a codonlocation that is
            # larger than the start, they are ALREADY in frame, so
            # the bigger one is always the correct stopcodon.
            for stopindex in indexliststop:
                if stopindex > counter:
                    # The counter here is still the same as the startlocation
                    startpoint = counter
                    stoppoint = stopindex + 3
                    orflist_spans.append([startpoint, stoppoint])
                    # We change the counter to be the same as the stopindex,
                    # so that we start counting from the end of the previous ORF
                    counter = stopindex
                    break
                elif counter > indexliststop[-1]:
                    counter += 10000000

        else:
            # increasing counter by 1 to increment the loop further.
            counter += 1
    return orflist_spans


def giveorfsequences(sequence, spanlist):
    """
    This function will actually give the sequences of the ORFs
    by slicing the sequence according to the span of the ORF locations.
    returns a list containing the orf sequences.
    """
    orflist = []
    for startstop in spanlist:
        orflist.append(sequence[startstop[0]:startstop[1]] + "*")
    return orflist


def findorf(inputseq):
    """
    this function puts everything together using the other functions
    in order to locate the ORFs.
    """
    # Preparation, making sure we have 2 sequences, the normal
    # one and the complementary strand sequence.
    sequence = inputseq[0].upper()
    revseq = sequence[::-1].upper()
    transdict = {
        "A": "T",
        "T": "A",
        "C": "G",
        "G": "C"}
    compseq = ""
    # here we set the Regular expression queries we want to use to find the start and stop codons.
    stopcodons = "(TAA|TGA|TAG)"
    startcodon = "ATG"
    # using the dict here to convert the sequence to its complementary counterpart
    for item in revseq:
        compseq += transdict[item]
    # Starting with RE from here
    regexstart = re.compile(startcodon)
    regexstop = re.compile(stopcodons)

    # using above mentioned function to make the list of
    # indexes where the start and stop codons reside.
    indexlists = makeindexlist(regexstart, regexstop, sequence)
    indexstartlist = indexlists[0]
    indexstoplist = indexlists[1]

    # idem for the complementary strand.
    revindexlists = makeindexlist(regexstart, regexstop, compseq)
    revindexstartlist = revindexlists[0]
    revindexstoplist = revindexlists[1]

    # dividing the lists into their respective reading frames.
    frameliststart = createframelists(indexstartlist)
    frameliststop = createframelists(indexstoplist)

    # idem for compl strand.
    framelistrevstart = createframelists(revindexstartlist)
    framelistrevstop = createframelists(revindexstoplist)

    # putting them into their actual spans, essentially making orf locations.

    listoforfspans1 = findspans(sequence, frameliststart[0], frameliststop[0])
    listoforfspans2 = findspans(sequence, frameliststart[1], frameliststop[1])
    listoforfspans3 = findspans(sequence, frameliststart[2], frameliststop[2])

    listofrevorfspans1 = findspans(compseq, framelistrevstart[0], framelistrevstop[0])
    listofrevorfspans2 = findspans(compseq, framelistrevstart[1], framelistrevstop[1])
    listofrevorfspans3 = findspans(compseq, framelistrevstart[2], framelistrevstop[2])

    # finding the actual sequences.
    list1 = giveorfsequences(sequence, listoforfspans1)
    list2 = giveorfsequences(sequence, listoforfspans2)
    list3 = giveorfsequences(sequence, listoforfspans3)

    revlist1 = giveorfsequences(compseq, listofrevorfspans1)
    revlist2 = giveorfsequences(compseq, listofrevorfspans2)
    revlist3 = giveorfsequences(compseq, listofrevorfspans3)

    orflist = list1 + list2 + list3
    revorflist = revlist1 + revlist2 + revlist3

    # removing the orfs that are shorter than 100 nucleotides, since that can not be a viable gene.
    finalorflist = []
    finalrevorflist = []
    for item in orflist:
        if len(item) > 100:
            finalorflist.append(item)

    # making a string to write to a file in fasta format, making a
    # header after every stopcodon found.
    forwardseq = ""
    numplus = 0
    for item in finalorflist:
        forwardseq += item

    # using inputseq[1] here because it contains the header from the original file
    newforwardseq = inputseq[1]
    for num, item in enumerate(forwardseq):
        if not item == "*" and not (num + numplus) % 80 == 79:
            newforwardseq += item

        if item == "*":
            newforwardseq += "\n> ORF nr. {}\n".format(num)
            numplus = 79 - num

        elif (num + numplus) % 80 == 79:
            newforwardseq += item
            newforwardseq += "\n"
    newforwardseq = newforwardseq[:-2]

    for item in revorflist:
        if len(item) > 100:
            finalrevorflist.append(item)

    reverseseq = ""
    numplus = 0
    for item in finalrevorflist:
        reverseseq += item

    newreverseseq = inputseq[1]
    for num, item in enumerate(reverseseq):
        if not item == "*" and not (num + numplus) % 80 == 79:
            newreverseseq += item

        if item == "*":
            newreverseseq += "\n> ORF nr. {}\n".format(num)
            numplus = 79 - num

        elif (num + numplus) % 80 == 79:
            newreverseseq += item
            newreverseseq += "\n"
    newreverseseq = newreverseseq[:-2]
    return [newforwardseq, newreverseseq]



def create_output_file(outputname, newforwardseq, newreverseseq):
    """
    Schrijft de output van het programma naar de respectievelijke files voor forward en reverse.
    """
    openfile1 = open(outputname + "forwardorfoutput", "w")
    openfile1.write(newforwardseq)

    openfile2 = open(outputname + "reverseorfoutput", "w")
    openfile2.write(newreverseseq)

def find_orf_multicore(cores, sequenceheaders_l):
    """
    Zorgt ervoor dat het programma met meerdere cores uitgevoerd kan worden
    """
    pool = Pool(cores)
    seqlist = pool.map(findorf, sequenceheaders_l)
    return seqlist


def main():
    """
    voert de functies uit en print de tijd dat het duurt +
    alle values uit de items van de orflists.
    :return:
    """
    start = time.time()

    args = arguments()
    fasta_file = args.files
    outputname = args.outputname
    sequenceheaders_l = open_file(fasta_file)

    seqlist = find_orf_multicore(args.cores, sequenceheaders_l)

    create_output_file(outputname, seqlist[0][0], seqlist[0][1])

    end = time.time()
    react_time = end - start
    print('it took the orf_finder', react_time)

if __name__ == "__main__":
    main()
